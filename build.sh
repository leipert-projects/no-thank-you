#!/usr/bin/env bash
set -euo pipefail
shopt -s globstar nullglob

TRANSLATIONS=()
LANGUAGES=()
function join_by { local d=$1; shift; echo -n "$1"; shift; printf "%s" "${@/#/$d}"; }

# Building a list of languages
for translationFile in translations/*.env; do
  # shellcheck source=/dev/null
  source "$translationFile"
  LANGUAGE=$(basename "$translationFile" ".env")
  TRANSLATIONS+=("<a href='../${LANGUAGE}/'>${NAME}</a>")
  LANGUAGES+=("'${LANGUAGE}'")
done
TRANSLATIONS_JOINED=$(join_by " &middot; " "${TRANSLATIONS[@]}" )

## Building each language index.html
for translationFile in translations/*.env; do
  LANGUAGE=$(basename "$translationFile" ".env")
  # shellcheck source=/dev/null
  source "$translationFile"
  mkdir -p "public/$LANGUAGE"
  
  LIST_ITEMS=""
  for listItem in "$NOT_LOOKING" "$HAPPY" "$NOONE_ELSE" "$NOT_HIRING"; do
    [ -n "$listItem" ] && LIST_ITEMS="$LIST_ITEMS<li>$listItem</li>"
  done

  export LANGUAGE TITLE NO_SOLICITING ADDRESS LIST_ITEMS TRANSLATIONS_JOINED LANGUAGES
  envsubst < template.html > "public/$LANGUAGE/index.html"
done

## Add user language redirect to main page via JS
LANGUAGES_JOINED=$(join_by ", " "${LANGUAGES[@]}" )
sed -e "s#\\.\\./#./#g" "public/en/index.html" |
  sed -e"s#</script>#var l=(navigator.language||navigator.userLanguage).substr(0,2).toLowerCase();[$LANGUAGES_JOINED].indexOf(l)>=0\\&\\&(location.href=location.href.replace(/\\\\/?$/,'/'+l));</script>#" > "public/index.html"

## gzip all files in public
find public -iname "*.gz" -print0 | xargs -0 rm -f
find public -type f -print0 | xargs -0 gzip -f -k
